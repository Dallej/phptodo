<?php
$db = null;
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Todo-list</title>
  </head>

  <body>
  </div id="content">
  <?php

  try {
    $db = new PDO('mysql:host=localhost;dbname=taskmanager;charset=utf8','root','');
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    //avataan tietokanta yhteys, linkki, nimi, merkistö, username, pw
  }catch(PDOExpection $pdoex){
    print "Tietokannan avaus epäonnistui" . $pdoex->getMessage();
    exit;
    //kerrotaan errori
  }
  